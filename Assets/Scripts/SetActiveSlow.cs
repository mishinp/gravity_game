﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetActiveSlow : MonoBehaviour
{
    private Image _image;
    private Text _text;
    private SpriteRenderer _srenderer;
    public float targetValue = 255.0f;
    private delegate void _delegate();
    _delegate dlg, dlg1;
    private void Awake()
    {
        _image = GetComponent<Image>();
        _text = GetComponent<Text>();
        _srenderer = GetComponent<SpriteRenderer>();
        if (_image != null)
        {
            dlg = SetAlphaForImage;
            dlg1 = SetAlphaSetImageNull;
        }
        if (_text != null)
        {
            dlg = SetAlphaForText;
            dlg1 = SetAlphaSetTextNull;
        }
        if (_srenderer != null)
        {
            dlg = SetAlphaForRenderer;
            dlg1 = SetAlphaSetRendererNull;
        }

    }
    // Start is called before the first frame update
    void Start()
    {
        

    }

    // Update is called once per frame
    void Update()
    {
        dlg();
    }

    void SetAlphaForImage()
    {
        _image.color = Vector4.Lerp(_image.color,
            new Vector4(_image.color.r, _image.color.g, _image.color.b, 1), Time.deltaTime);
    }

    void SetAlphaForText()
    {
        _text.color = Vector4.Lerp(_text.color,
            new Vector4(_text.color.r, _text.color.g, _text.color.b, targetValue/255.0f), Time.deltaTime);
    }
    void SetAlphaForRenderer()
    {
        _srenderer.color= Vector4.Lerp(_srenderer.color,
            new Vector4(_srenderer.color.r, _srenderer.color.g, _srenderer.color.b, 1), Time.deltaTime);
    }

    void SetAlphaSetImageNull()
    {
        _image.color = new Vector4(_image.color.r, _image.color.g, _image.color.b, 0);
    }
    void SetAlphaSetTextNull()
    {
        _text.color = new Vector4(_text.color.r, _text.color.g, _text.color.b, 0);
    }
    void SetAlphaSetRendererNull()
    {
        _srenderer.color = new Vector4(_srenderer.color.r, _srenderer.color.g, _srenderer.color.b, 0);
    }
    private void OnEnable()
    {
        dlg1();
    }
    public void Respawn()
    {
        StartCoroutine(StartRespawn());
    }
    private IEnumerator StartRespawn()
    {
        yield return new WaitForSeconds(1f);
        _image.color = new Vector4(_image.color.r, _image.color.g, _image.color.b, 0);
        this.gameObject.SetActive(false);
    }
}
