﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Simple_rotate : MonoBehaviour
{
    [SerializeField]
    private float rotateSpeed;

    public float xAngle, yAngle, zAngle;

    // Update is called once per frame
    void Update()
    {
        //this.gameObject.transform.Rotate += new Vector3(1f, 1f, 1f);
        transform.Rotate(xAngle, yAngle, zAngle, Space.World);
    }
}
