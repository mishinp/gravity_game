﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WiggleControl : MonoBehaviour
{

    [SerializeField] bool x = false;
    [SerializeField] bool y = false;
    [SerializeField] bool z = false;


    [Range(-5f, 5f)]
    [SerializeField] float value;

    [Range(0, 5f)]
    [SerializeField] float interval;


    void Update()
    {
        if (x)
        {
            wiggleX();
            x = !x;
        }

        if (y)
        {
            wiggleY();
            y = !y;
        }

        if (z)
        {
            wiggleZ();
            z = !z;
        }
    }


    void wiggleX()
    {
        iTween.MoveBy(gameObject, iTween.Hash("x", value, "time", interval, "easeType", "easeInOutSine", "loopType", "pingPong", "Delay", 0.1));
    }
    void wiggleY()
    {
        iTween.MoveBy(gameObject, iTween.Hash("y", value, "time", interval, "easeType", "easeInOutSine", "loopType", "pingPong","Delay", 0.1));
    }

    void wiggleZ()
    {
        iTween.MoveBy(gameObject, iTween.Hash("z", value, "time", interval, "easeType", "easeInOutSine", "loopType", "pingPong", "Delay", 0.1));
    }


}
