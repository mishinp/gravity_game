﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTelekinesis : MonoBehaviour
{
    public bool _UsiWave = false;
    public GameObject _Wave;

    [HideInInspector]
    public GameObject _Target;
    [HideInInspector]
    public Vector3 _TargetLevitation;
    [HideInInspector]
    public Vector3 screenPosition;
    [HideInInspector]
    public Vector3 currentScreenSpace;
    [HideInInspector]
    public Vector3 offset;
    [HideInInspector]
    public Vector3 currentPosition;
    [HideInInspector]
    public bool _Telekinesis=false;
    [HideInInspector]
    public Rigidbody _Rigidbody;
    [HideInInspector]
    public float firstRay;
    [HideInInspector]
    public Transform _Camera;
    [HideInInspector]
    public Vector3 direction;
    [HideInInspector]
    public bool _Collision;
    // Start is called before the first frame update
    virtual public void Start()
    {
        _TargetLevitation = GetComponent<Levitation>()._TargetPosition.transform.position;
        currentPosition = _TargetLevitation;
        _Target = GetComponent<Levitation>()._TargetPosition.gameObject;
        _Rigidbody = GetComponent<Rigidbody>();
        _Camera = Camera.main.transform;
    }

    // Update is called once per frame
    virtual public void Update()
    {
        if (_Telekinesis) Steering();
    }
    virtual public void ReactToHit()
    {
        if (!_Telekinesis&&((firstRay+0.5f)<Time.time))
        {
            GetComponent<Levitation>()._Levitation = true;
            firstRay = Time.time;
            _Telekinesis = true;
            _Wave.SetActive(true);
            _Target.transform.position = transform.position;
            _Target.transform.SetParent(_Camera);
        }
        /*else if (_Telekinesis&&((firstRay+1)<Time.time))
        {
            firstRay = Time.time;
            _Telekinesis = false;
            _Target.transform.SetParent(null);
           

        }*/
    }
    virtual public void Steering()
    {
        if (Input.GetKey(KeyCode.X))
        {
            direction = Vector3.Normalize(_Target.transform.position - _Camera.position);
            if ((_Target.transform.position - _Camera.position).magnitude < 25.0f)
            {
                _Target.transform.position = _Target.transform.position + direction * Time.deltaTime * 10.0f;
            }
        }
        if (Input.GetKey(KeyCode.Z))
        {
            direction = Vector3.Normalize(_Target.transform.position - _Camera.position);
            if ((_Target.transform.position - _Camera.position).magnitude > 4.0f)
            {
                _Target.transform.position = _Target.transform.position - direction * Time.deltaTime * 10.0f;
            }
        }
        if (!Input.GetKey(KeyCode.Mouse0))
        {
            _Telekinesis = false;
            _Target.transform.SetParent(null);
            _Wave.SetActive(false);
        }
    }

}
