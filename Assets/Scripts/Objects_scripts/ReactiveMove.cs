﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactiveMove : MonoBehaviour
{
    
    private Vector3 _startPosition;

    [SerializeField] bool freezeRotation;
    [SerializeField] bool enableGravity;
    

    private bool gravityChange = true;


    [SerializeField] bool enableX;
    [SerializeField] bool clampX;
    [SerializeField] Vector2 xRange = new Vector2(-100, 100);
    //private float _xCurrent;

    [SerializeField] bool enableY;
    [SerializeField] bool clampY;
    [SerializeField] Vector2 yRange = new Vector2(-100, 100);

    [SerializeField] bool enableZ;
    [SerializeField] bool clampZ;
    [SerializeField] Vector2 zRange = new Vector2(-100, 100);


    private Vector3 screenPosition;
    private bool isMouseDrag;
    private Vector3 currentPosition;
    private Vector3 currentScreenSpace;
    //private GameObject target;
    private Vector3 offset;

    private bool _onObject;


    private void Start()
    {
        _startPosition = gameObject.transform.position;
        currentPosition = _startPosition;
    }
    private void Update()
    {
        //включаем или отключаем гравитацию вызывая фугкцию
        if (gravityChange)
        {
            ControlGravity();
            gravityChange = !gravityChange;
            
        }

        if (Input.GetMouseButtonUp(0))
        {
            //это срабатывает когдмы отпускаем мышку ВЕЗДЕ
            isMouseDrag = false;
            //Debug.Log("отпустил мышку в космосе");

        }
        if (clampX)
        {
            currentPosition.x = Mathf.Clamp(currentPosition.x, _startPosition.x+xRange.x, _startPosition.x + xRange.y);
        }

        if (clampY)
        {
            currentPosition.y = Mathf.Clamp(currentPosition.y, _startPosition.y + yRange.x, _startPosition.y + yRange.y);
        }

        if (clampZ)
        {
            currentPosition.z = Mathf.Clamp(currentPosition.z, _startPosition.z + zRange.x, _startPosition.z + zRange.y);
        }
        transform.position = currentPosition;
    }
    public virtual void ReactToHit()
    {

        

        if (Input.GetMouseButtonDown(0)) {

            isMouseDrag = true;


            //Convert world position to screen position.
            screenPosition = Camera.main.WorldToScreenPoint(transform.position);
            offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPosition.z));
            _onObject = true;

        } else
        {
            //это работает когда курсор над объектом
            
           
            //Debug.Log("_onObject" + _onObject);
        }

        if (Input.GetMouseButtonUp(0) || !_onObject)
        {
            //это срабатывает когдмы отпускаем мышку, ТОЛЬКО НАД ОБЪЕКТОМ
            isMouseDrag = false;
            
            //Debug.Log("отпустил мышку над объектом");

        }

        if (isMouseDrag)
        {

            //track mouse position.
            screenPosition = Camera.main.WorldToScreenPoint(transform.position);
            currentScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPosition.z);

            //convert screen position to world position with offset changes.
            currentPosition = Camera.main.ScreenToWorldPoint(currentScreenSpace) + offset;


            if (!enableX)
            {
                currentPosition.x = _startPosition.x;
                
            }

            if (!enableY)
            {
                currentPosition.y = _startPosition.y;
                
            }

            if (!enableZ)
            {
                currentPosition.z = _startPosition.z;
                
            }


            //тут надо как-то ограничить


            //It will update target gameobject's current postion.
            
        }

        
    }

    

    public void ControlGravity()
    {
        
        if (enableGravity)
        {
            this.gameObject.GetComponent<Rigidbody>().useGravity = true;
            this.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        }

        if (!enableGravity)
        {
            this.gameObject.GetComponent<Rigidbody>().useGravity = false;
            this.gameObject.GetComponent<Rigidbody>().isKinematic = true;
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        if (clampX)
            Gizmos.DrawLine(transform.position + new Vector3(xRange.x,0,0),transform.position+ new Vector3(xRange.y, 0, 0));
        if (clampY)
            Gizmos.DrawLine(transform.position + new Vector3(0,yRange.x,  0), transform.position + new Vector3(0, yRange.y, 0));
        if (clampZ)
            Gizmos.DrawLine(transform.position + new Vector3(0, 0, zRange.x), transform.position + new Vector3(0, 0, zRange.y));
    }


}
