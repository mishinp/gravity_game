﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactiveScale : MonoBehaviour
{
 
    [SerializeField] bool scaleX;
    [SerializeField] bool scaleY;
    [SerializeField] float maxScale=4.0f;
    [SerializeField] float minScale=0.5f;
    //[SerializeField] Vector3 defaultRotateSpeed;

    [Range(-10,1000)]
    public float scaleSpeed;
  
    private bool isMouseDrag;
    

    private bool _onObject;

    private Vector3 _targetScale;


    private void Start()
    {
        _targetScale = transform.localScale;
    }
    private void Update()
    {
        
        
        if (Input.GetMouseButtonUp(0))
        {
            //это срабатывает когдмы отпускаем мышку ВЕЗДЕ
            isMouseDrag = false;
            //Debug.Log("отпустил мышку в космосе");
            
        }
        transform.localScale = Vector3.Lerp(transform.localScale, _targetScale, Time.deltaTime*4.0f);

    }

    public void ReactToHit_scale()
    {

        if (scaleX)
        {
            Debug.Log("scale process X here");
            float X = Input.GetAxis("Mouse X") * scaleSpeed * Time.deltaTime;
            _targetScale += new Vector3(X, X, X);
            _targetScale = Vector3.ClampMagnitude(_targetScale, maxScale);//horizontal scale
            if (_targetScale.x < minScale) _targetScale = new Vector3(minScale, minScale, minScale);
        }


        if (scaleY)
        {
            Debug.Log("scale process Y here");
            float Y = Input.GetAxis("Mouse Y") * scaleSpeed * Time.deltaTime;
            _targetScale += new Vector3(Y, Y, Y); //vertical scale
            _targetScale = Vector3.ClampMagnitude(_targetScale, maxScale);//horizontal scale
            if (_targetScale.x < minScale) _targetScale = new Vector3(minScale, minScale, minScale);
        }
        Debug.Log("ReactToHit");

        /*if (Input.GetMouseButtonDown(0)) {

            isMouseDrag = true;
            

        } 
        else
        {
            //это работает когда курсор над объектом
            _onObject = true;
            
        }

        if (Input.GetMouseButtonUp(0) || !_onObject)
        {
            //это срабатывает когдмы отпускаем мышку, ТОЛЬКО НАД ОБЪЕКТОМ
            isMouseDrag = false;
            
           
        }

        if (isMouseDrag)
        {

            if (scaleX)
            {
                Debug.Log("scale process X here");
                float X = Input.GetAxis("Mouse X") * scaleSpeed * Time.deltaTime;
                transform.localScale += new Vector3(X, X, X); //horizontal scale
            }

            
            if (scaleY)
            {
                Debug.Log("scale process Y here");
                float Y = Input.GetAxis("Mouse Y") * scaleSpeed * Time.deltaTime;
                transform.localScale += new Vector3(Y, Y, Y); //vertical scale
            }
            
        }*/

        
    }






}
