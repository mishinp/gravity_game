using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class RotateController : MonoBehaviour
{

    [SerializeField] bool rotX;
    [SerializeField] bool rotY;

    [SerializeField] Vector3 defaultRotateSpeed;
    
    
    public float rotationSpeed;
    //public float speed;

    private float rotSpeed;

    private float _rotSpeedX;
    private float _rotSpeedY;
    private float _rotSpeedZ;

    private void Start()
    {
        rotationSpeed = 5;
    }

    private void Update()
    {
        _rotSpeedX = defaultRotateSpeed.x * Time.deltaTime;
        _rotSpeedY = defaultRotateSpeed.y * Time.deltaTime;
        _rotSpeedZ = defaultRotateSpeed.z * Time.deltaTime;

        transform.Rotate(_rotSpeedX, _rotSpeedY, _rotSpeedZ);

        PetyaLogic();
    }


    private void OnMouseDrag2()
    {
        float X = Input.GetAxis("Mouse X") * rotationSpeed;
        float Y = Input.GetAxis("Mouse Y") * rotationSpeed;

        transform.Rotate(new Vector3(0, -1, 0), X); //horizontal rotate
        transform.Rotate(new Vector3(1, 0, 0), Y); //vertical rotate
    }

    private void PetyaLogic()
    {
        if (!Input.GetMouseButton(0))
            return;

        if (rotX) { 
            float X = Input.GetAxis("Mouse X") * rotationSpeed;
            transform.Rotate(new Vector3(0, -1, 0), X); //horizontal rotate
        }

        if (rotY) {
            float Y = Input.GetAxis("Mouse Y") * rotationSpeed;
            transform.Rotate(new Vector3(1, 0, 0), Y); //vertical rotate
        }
    }




}

