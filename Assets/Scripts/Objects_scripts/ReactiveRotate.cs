﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactiveRotate : MonoBehaviour
{
 
    [SerializeField]public bool rotX;
    [SerializeField]public bool rotY;
    [SerializeField]public bool rotZ;

    //[SerializeField] Vector3 defaultRotateSpeed;

    [Range(10,1000)]
    public float rotationSpeed;
    
    [HideInInspector]
    public bool isMouseDrag;
    [HideInInspector]
    public GameObject target;

    [HideInInspector]
    public bool _onObject;
    [HideInInspector]
    public Vector3 _curDir;
    

    private void Start()
    {
        //_TargetRot = Instantiate<GameObject>(this.gameObject);
    }

    private void Update()
    {
        
        
        if (Input.GetMouseButtonUp(0))
        {
            //это срабатывает когдмы отпускаем мышку ВЕЗДЕ
            isMouseDrag = false;
            //Debug.Log("отпустил мышку в космосе");
            MoveTelekinesisMan._curCub = null;
        }
        //transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.EulerAngles(_targetRot), Time.deltaTime);
    }


    public virtual void ReactToHit_rotate(Vector3 hit)
    {

        Debug.Log("ReactToHit");

        if (Input.GetMouseButtonDown(0)) {
            MoveTelekinesisMan._curCub = this.gameObject;
            isMouseDrag = true;
             _onObject = true;

        } 
     

        if (Input.GetMouseButtonUp(0) || !_onObject)
        {
            //это срабатывает когдмы отпускаем мышку, ТОЛЬКО НАД ОБЪЕКТОМ
            isMouseDrag = false;
            


        }

        if (isMouseDrag)
        {

            if (rotX)
            {   

                float X = Input.GetAxis("Mouse X") * rotationSpeed;
                transform.Rotate(new Vector3(0, -1, 0), X); //horizontal rotate
                Messenger.Broadcast("Change");
            }

            if (rotY)
            {
                float Y = Input.GetAxis("Mouse Y") * rotationSpeed;
                transform.Rotate(new Vector3(1, 0, 0), Y); //vertical rotate
                Messenger.Broadcast("Change");
            }
            if (rotZ)
            {
                
                float Angle = Mathf.Clamp(Vector3.Angle(hit-transform.position, _curDir),0,15);
                _curDir = hit - transform.position;
                float Z = Angle*1.2f;
                transform.Rotate(new Vector3(0, 0, 1), Z);
                
            }
        }

        
    }



    

    

}
