﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Levitation : MonoBehaviour
{   
    [HideInInspector]
    public Transform _TargetPosition;
    public float _MaxSpeed = 5.0f;
    public float _MaxAmplitude = 0.2f;
    public float _AngularSpeed = 0.5f;
    public float _SpeedColebation = 1.0f;
    public bool _ClampRotation = false;
    public bool _Levitation=true;

    private Rigidbody _Rigidbody;
    private Transform _Transform;
    private Vector3 _VecTargetTemp;
    private Vector3 _randVec;
    private Vector3 _Force;
    private Quaternion StartRotation;
    // Start is called before the first frame update
    void Start()
    {
        _Transform = GetComponent<Transform>();
        _Rigidbody = GetComponent<Rigidbody>();
        _TargetPosition = Instantiate<GameObject>(new GameObject("TargetLevitation"), _Transform.position, Quaternion.identity).transform;
        _randVec=new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
        if (!_ClampRotation) _Rigidbody.angularVelocity = _randVec * _AngularSpeed;
    }

    // Update is called once per frame
    void Update()
    {   
        if (_Levitation)
        {
            _VecTargetTemp = _TargetPosition.position + (new Vector3(0, _MaxAmplitude * Mathf.Sin(Time.timeSinceLevelLoad*_SpeedColebation+Random.Range(0.0f,5.0f)), 0));

            //_Rigidbody.MovePosition(Vector3.Lerp(_Rigidbody.position,_VecTargetTemp,Time.deltaTime*4f));
            _Rigidbody.AddForceAtPosition((_VecTargetTemp - _Rigidbody.position)*10.0f, _VecTargetTemp, ForceMode.Force);
        }
       

        
    }
    
    private void OnCollisionEnter(Collision collision)
    {   
        if (!_ClampRotation)
        {
            _Rigidbody.angularVelocity = Vector3.zero;
            _Rigidbody.velocity = Vector3.zero;
        }
        
    }
    private void OnCollisionExit(Collision collision)
    {
        if (!_ClampRotation)
        {
            _Rigidbody.angularVelocity = _randVec * _AngularSpeed;
        }

        
    }
}
