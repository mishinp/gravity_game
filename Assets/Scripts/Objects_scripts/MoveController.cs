﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshCollider))]

public class MoveController : MonoBehaviour
{

    private Vector3 _screenPoint;
    private Vector3 _offset;

    private Vector3 _startPosition;

    [SerializeField] bool freezeRotation;
    [SerializeField] bool enableGravity;

    private bool gravityChange = true;


    [SerializeField] bool enableX;
    [SerializeField] bool clampX;
    [SerializeField] Vector2 xRange = new Vector2(-100, 100);
    //private float _xCurrent;

    [SerializeField] bool enableY;
    [SerializeField] bool clampY;
    [SerializeField] Vector2 yRange = new Vector2(-100, 100);

    [SerializeField] bool enableZ;
    [SerializeField] bool clampZ;
    [SerializeField] Vector2 zRange = new Vector2(-100, 100);


    private void Update()
    {
        //включаем или отключаем гравитацию вызывая фугкцию

        if (gravityChange)
        {
            ControlGravity();
            gravityChange = !gravityChange;
            Debug.Log("gravity changed");
        } 
        
        


    }

    void OnMouseDown()

    {

        _startPosition = gameObject.transform.position;

        if (!enableGravity)
        {
            
            
        }
        
        
        //заморозка врещения объекта
        if (freezeRotation)
        {
            GetComponent<Rigidbody>().freezeRotation = true;
        }   
            
        //Transforms position from world space into screen space.
        _screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);

        _offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z));



    }

    void OnMouseDrag()
    {
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z);

        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + _offset;


        if (!enableX) { 
            curPosition.x = _startPosition.x;
            Debug.Log("x - disable");
        }

        if (!enableY)
        {
            curPosition.y = _startPosition.y;
            Debug.Log("y - disable");
        }

        if (!enableZ)
        {
            curPosition.z = _startPosition.z;
            Debug.Log("z - disable");
        }


        //тут надо как-то ограничить


        if (clampX) { 
            curPosition.x = Mathf.Clamp(curPosition.x, xRange.x, xRange.y);
        }

        if (clampY) { 
            curPosition.y = Mathf.Clamp(curPosition.y, yRange.x, yRange.y);
        }

        if (clampZ) { 
            curPosition.z = Mathf.Clamp(curPosition.z, zRange.x, zRange.y);
        }


        transform.position = curPosition;

    }

    public void ControlGravity()
    {

        if (enableGravity)
        { 
            this.gameObject.GetComponent<Rigidbody>().useGravity = true;
            this.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        }

        if (!enableGravity)
        {
            this.gameObject.GetComponent<Rigidbody>().useGravity = false;
            this.gameObject.GetComponent<Rigidbody>().isKinematic = true;
        }
    }

    

    

}