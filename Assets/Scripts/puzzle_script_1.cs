﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class puzzle_script_1 : MonoBehaviour
{


    //принять используемые объекты

    [SerializeField] GameObject element1;

    [SerializeField] GameObject element2;

    [SerializeField] GameObject element3;

    //принять объект - результат

    [SerializeField] GameObject resultObj1;


    //принять целевые положения/вращения объектов 
    [SerializeField] Vector3 elem1_target;
    [SerializeField] Vector3 elem2_target;
    [SerializeField] Vector3 elem3_target;

    //принять допустимые диапозоны неточности
    [Range(0, 3)] [SerializeField] float gap;

    private bool _puzzle1;
    private bool _checking;

    private Animator _animator;


    // Start is called before the first frame update
    void Start()
    {
        _puzzle1 = false;
        _checking = true;
        _animator = resultObj1.gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Compare(element1, elem1_target) && Compare(element2, elem2_target) && Compare(element3, elem3_target) && _checking)
        {
            Debug.Log("objects is OK");
            _puzzle1 = true;
            _checking = false;
        }

        Puzzle1Complete();
    }

    private bool Compare(GameObject item, Vector3 target)
    {

        float dist = Vector3.Distance(item.transform.localPosition, target);
        
        //Debug.Log("element1 " + dist);
        

        
        if (dist <= gap)
        { 
            return true;
        }

        else
        {
            return false;
        }
    }

    private void Puzzle1Complete()
    {

        if (_puzzle1) {
            resultObj1.gameObject.SetActive(true);
            _puzzle1 = !_puzzle1;
            Debug.Log("puzzle complete");
            _animator.SetBool("animation", true);
            
        }
    }
}
