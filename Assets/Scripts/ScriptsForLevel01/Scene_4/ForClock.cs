﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForClock : ReactiveRotate
{
    public GameObject _Hours;
    public GameObject _ColliderDeth;
    delegate void Steering();
    Steering _Steering;
    public static float _CurTime;
    // Start is called before the first frame update
    void Start()
    {
        _Steering += TraficTram;
    }

    // Update is called once per frame
    void Update()
    {
        if (_Steering != null) _Steering();
    }

    public override void ReactToHit_rotate(Vector3 hit)
    {
        _ColliderDeth.SetActive(false);
        _Steering -= TraficTram;
        Debug.Log("ReactToHit");

        if (Input.GetMouseButtonDown(0))
        {
            MoveTelekinesisMan._curCub = this.gameObject;
            isMouseDrag = true;
            _onObject = true;

        }


        if (Input.GetMouseButtonUp(0) || !_onObject)
        {
            //это срабатывает когдмы отпускаем мышку, ТОЛЬКО НАД ОБЪЕКТОМ
            isMouseDrag = false;



        }

        if (isMouseDrag)
        {
            
            if (rotX)
            {

                float X = Input.GetAxis("Mouse X") * rotationSpeed;
                transform.Rotate(new Vector3(0, -1, 0), X); //horizontal rotate
                Messenger.Broadcast("Change");
            }

            if (rotY)
            {
                float Y = Input.GetAxis("Mouse Y") * rotationSpeed;
                transform.Rotate(new Vector3(1, 0, 0), Y); //vertical rotate
                Messenger.Broadcast("Change");
            }
            if (rotZ)
            {
                
                float Angle = Mathf.Clamp(Vector3.SignedAngle(hit - transform.position, _curDir,new Vector3(0,0,1)), -15, 15);
               
                _curDir = hit - transform.position;
                float Z = Angle * 1.2f;
                _CurTime += Z*0.1f;
                transform.Rotate(new Vector3(0, 0, 1), Z);
                _Hours.transform.Rotate(new Vector3(0, 0, 1), Z / 12);
            }
        }


    }
    private void TraficTram()
    {
        
        _CurTime = Time.time;
       // transform.Rotate(new Vector3(0, 0, 1), _CurTime/2.0f);
       // _Hours.transform.Rotate(new Vector3(0, 0, 1), _CurTime / 24.0f);
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, _CurTime / 4.0f);
        _Hours.transform.localEulerAngles = new Vector3(_Hours.transform.localEulerAngles.x, _Hours.transform.localEulerAngles.y, _CurTime / 48.0f);
    }
}
