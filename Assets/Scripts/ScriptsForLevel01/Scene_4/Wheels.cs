﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wheels : MonoBehaviour
{
    public bool x=false;
    public bool z=true;
    Transform _transform;
    private delegate void Steering();
    Steering _steering;
    // Start is called before the first frame update
    void Start()
    {
        _transform = GetComponent<Transform>();
        if (x==true)
        {
            _steering=rotateX;
        }
        else
        {
            _steering=rotateY;
        }
    }

    // Update is called once per frame
    void Update()
    {
        _steering();
    }
    private void rotateX()
    {
        _transform.localRotation = Quaternion.EulerAngles(new Vector3(0, 0, _transform.position.z));
    }
    private void rotateY()
    {
        _transform.localRotation = Quaternion.EulerAngles(new Vector3(0, 0, _transform.position.x));
    }
}
