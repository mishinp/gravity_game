﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buses : MonoBehaviour
{
    Vector3 firstPoint;
    Vector3 Direction;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ReactToHitBuses(Vector3 hit)
    {
        Debug.Log("HitBuses");
        Direction = Vector3.ClampMagnitude(firstPoint-hit, 1.0f);
        Direction.y = 0;
        Direction.z = 0;
        transform.Translate(Direction);
        firstPoint = hit;
    }
}
