﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trams : MonoBehaviour
{
    private float _BirthCurTime;
    private Vector3 _StartPos;
    [HideInInspector]
    public bool _LeftInst = false;
    [HideInInspector]
    public bool _RightInst = false;
    // Start is called before the first frame update
    void Start()
    {
        _BirthCurTime = ForClock._CurTime;
        _StartPos = transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, _StartPos.z + (ForClock._CurTime - _BirthCurTime));
        if ((!_LeftInst)&&(transform.localPosition.z > -136))
        {
            _LeftInst = true;
            var curTram=Instantiate(gameObject, Vector3.zero, transform.rotation, gameObject.transform.parent);
            curTram.transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -263);
            curTram.GetComponent<Trams>()._LeftInst = false;
            curTram.GetComponent<Trams>()._RightInst = true;
        }
        if ((!_RightInst) && (transform.localPosition.z < -200))
        {
             _RightInst = true;
             var curTram = Instantiate(gameObject, Vector3.zero, transform.rotation, gameObject.transform.parent);
             curTram.transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -74);
             curTram.GetComponent<Trams>()._LeftInst = true;
             curTram.GetComponent<Trams>()._RightInst = false;
        }
        if ((transform.localPosition.z < -261) || (transform.localPosition.z > -73))
        {
            var meshRenderers = GetComponentsInChildren<MeshRenderer>();
            foreach (var i in meshRenderers)
            {
                i.enabled = false;
            }
        }
        else
        {
            var meshRenderers = GetComponentsInChildren<MeshRenderer>();
            foreach (var i in meshRenderers)
            {
                i.enabled = true;
            }
        }
    }
}
