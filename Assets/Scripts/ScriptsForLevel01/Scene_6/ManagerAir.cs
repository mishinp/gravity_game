﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerAir : MonoBehaviour
{
    public GameObject statProp;
    public GameObject levitProp;
    public Animator airPlane;

    private bool _Start = false;
    private delegate void _Steering();
    private _Steering steering;

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if (steering != null)
        {
            steering();
            return;
        }
        if (statProp.transform.rotation.eulerAngles.z > 170.0f)
        {
            AddToSteering();
        }

        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "propeller")
        {
            statProp.SetActive(true);
            levitProp.SetActive(false);
        }
    }
    private void StartAir()
    {
        statProp.transform.Rotate(new Vector3(0, 0, 360.0f*Time.deltaTime));
    }
    private void AddToSteering()
    {
        steering += StartAir;
        StartCoroutine(StartAirPlane());
    }
    private IEnumerator StartAirPlane()
    {
        //
        
        yield return new WaitForSeconds(2.0f);
        airPlane.SetTrigger("StartAir");
    }
}
