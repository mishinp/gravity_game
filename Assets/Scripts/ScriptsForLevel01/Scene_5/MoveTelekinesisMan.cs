﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTelekinesisMan : MoveTelekinesis
{
    static public GameObject _curCub;
    static public int Count = 14;
    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
    }

    public override void ReactToHit()
    {   
        if ((_curCub == null) || (_curCub.Equals(this.gameObject)))
        {
            _curCub = this.gameObject;
            
        }
        else
        {
            return;
        }
        base.ReactToHit();
        transform.SetParent(null);
    }
    public override void Steering()
    {
        if (Input.GetKey(KeyCode.X))
        {
            direction = Vector3.Normalize(_Target.transform.position - _Camera.position);
            if ((_Target.transform.position - _Camera.position).magnitude < 25.0f)
            {
                _Target.transform.position = _Target.transform.position + direction * Time.deltaTime * 10.0f;
            }
        }
        if (Input.GetKey(KeyCode.Z))
        {
            direction = Vector3.Normalize(_Target.transform.position - _Camera.position);
            if ((_Target.transform.position - _Camera.position).magnitude > 4.0f)
            {
                _Target.transform.position = _Target.transform.position - direction * Time.deltaTime * 10.0f;
            }
        }
        if (!Input.GetKey(KeyCode.Mouse0))
        {
            _Telekinesis = false;
            _Target.transform.SetParent(null);
            _Wave.SetActive(false);
            _curCub = null;
            Count--;
            Messenger.Broadcast("Change");

        }

    }
}
