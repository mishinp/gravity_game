﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadsManager : MonoBehaviour
{
    public GameObject _Woman;
    public GameObject _Man;
    public ScriptForBridgeScene5 _Bridge1;
    public ScriptForBridgeScene5 _Bridge2;
    private void Start()
    {
        Messenger.AddListener("Change", ChangeRotation);
    }

    public void ChangeRotation()
    {
        if (MoveTelekinesisMan.Count <= 0)
        {
            _Man.GetComponent<ScriptForBridgeScene5>().StartMoveBridge();
        }
        if (((_Woman.transform.localEulerAngles.y > (10))&&(_Woman.transform.localEulerAngles.y < (90)))&&
                (_Man.transform.localEulerAngles.y > (200)) && (_Man.transform.localEulerAngles.y < (280)) && (MoveTelekinesisMan.Count <= 0))
        {
            _Bridge1.StartMoveBridge();
            _Bridge2.StartMoveBridge();
        }

    }
}
