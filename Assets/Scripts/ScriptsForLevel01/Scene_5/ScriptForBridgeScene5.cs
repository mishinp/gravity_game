﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptForBridgeScene5 : MonoBehaviour
{
    public GameObject _TargetPos;

    private delegate void Sterring();
    private Sterring _Sterring;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (_Sterring != null) _Sterring();
    }
    public void StartMoveBridge()
    {
        _Sterring = MoveBridge;
        Debug.Log("MoveBridge");
    }
    public void MoveBridge()
    {
        transform.position = Vector3.Lerp(transform.position, _TargetPos.transform.position, Time.deltaTime);
    }
}
