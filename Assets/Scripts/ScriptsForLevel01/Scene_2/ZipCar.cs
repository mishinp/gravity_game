﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZipCar : ReactiveMove
{
    public GameObject _Stage1;
    public GameObject _Stage2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public override void ReactToHit()
    {
        gameObject.SetActive(false);
        _Stage2.SetActive(true);
       

    }
    IEnumerator FrizY()
    {
        yield return new WaitForSeconds(5.0f);
        _Stage2.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY;
        _Stage2.GetComponent<Levitation>().enabled = true;
        _Stage2.GetComponent<MoveTelekinesis>().enabled = true;
    }

}
