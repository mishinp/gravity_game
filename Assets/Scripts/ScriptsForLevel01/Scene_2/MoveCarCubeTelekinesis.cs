﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCarCubeTelekinesis : MonoBehaviour
{
    private void Start()
    {
        StartCoroutine(StCor());
    }
    IEnumerator StCor()
    {
        yield return new WaitForSeconds(0.1f);
        GetComponent<Levitation>()._Levitation = false;
    }
    IEnumerator StCor1()
    {
        yield return new WaitForSeconds(0.3f);
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
    }
    private void OnCollisionEnter(Collision collision)
    {
        StartCoroutine(StCor());
    }

    

}
