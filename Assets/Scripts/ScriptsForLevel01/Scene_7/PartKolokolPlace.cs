﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartKolokolPlace : MonoBehaviour
{
    public GameObject _Colocol;

    [HideInInspector]
    public static GameObject _ColocolAnim;

    [HideInInspector]
    static public int _Count = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == _Colocol)
        {   
            GetComponent<MeshRenderer>().enabled = true;
            Destroy(_Colocol);
            _Count++;
            Debug.Log(_Count);
            if (_Count == 6)
            {
                
                _ColocolAnim.SetActive(true);
                gameObject.transform.parent.gameObject.SetActive(false);
            }
        }
    }
}
