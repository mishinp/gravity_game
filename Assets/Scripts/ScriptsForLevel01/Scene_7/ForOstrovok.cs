﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForOstrovok : MonoBehaviour
{
    public Transform _Target;
    public string _Event;
    public delegate void Steering();
    [HideInInspector]
    public Steering _Sterring;
    public Rigidbody _Rigidbody;
    private CharacterController _Player;
    private Vector3 _Delta;
    private Vector3 _StartPosition;
    // Start is called before the first frame update
    void Start()
    {
        Messenger.AddListener(_Event, AddMoveOstov);
        _StartPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (_Sterring != null) _Sterring();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            //other.gameObject.transform.SetParent(this.gameObject.transform);
            _Player = other.gameObject.GetComponent<CharacterController>();
           //_Player.transform.SetParent(this.gameObject.transform);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.transform.SetParent(null);
        }
    }
    public void AddMoveOstov()
    {
        _Delta = (_Target.position - transform.position) / 80.0f;
        _Sterring += MoveOstrov;
    }

    private void MoveOstrov()
    {
        
        _Player.Move(_Delta*Time.deltaTime);
        transform.position = transform.position+_Delta*Time.deltaTime;
        if (transform.position.y>_Target.position.y)
            _Sterring -= MoveOstrov;

        // Vector3 LerpDelta = Vector3.Lerp(transform.position, _Target.position, Time.deltaTime / 50.0f) - transform.position;
        // _Player.Move(LerpDelta);
        // transform.position = Vector3.Lerp(transform.position, _Target.position, Time.deltaTime/50.0f);
        //_Rigidbody.velocity = (_Target.position - _Rigidbody.position) / Time.fixedDeltaTime;
        //Debug.Log("Move");
        
    }
    public void SetStartPosition()
    {
        transform.position = _StartPosition;
    }
    
}
