﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KolokolStartAnim : ReactiveMove
{
    public string Event;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public override void ReactToHit()
    {
        GetComponent<Animator>().SetTrigger("StartSong");
        Messenger.Broadcast(Event,MessengerMode.DONT_REQUIRE_LISTENER);
    }
}
