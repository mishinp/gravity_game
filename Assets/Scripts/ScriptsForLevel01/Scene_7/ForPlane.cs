﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForPlane : MonoBehaviour
{
    public GameObject _OstrovokPlatforma;
    private Rigidbody _RigidBody;
    // Start is called before the first frame update
    void Start()
    {
        _RigidBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        _RigidBody.velocity=(_OstrovokPlatforma.transform.position-_RigidBody.position);
    }
}
