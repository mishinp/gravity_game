﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptsForBus : MonoBehaviour
{
    static public bool Obstacle = false;
    public GameObject _Bus;

    private Animator _BusAnimator;
    // Start is called before the first frame update
    void Start()
    {
        _BusAnimator = _Bus.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (Obstacle == false)
            {
                _BusAnimator.SetTrigger("EnterWithoutObstacle");
            }
            else
            {
                _BusAnimator.SetTrigger("EnterWithObstacle");
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            _BusAnimator.SetTrigger("Exit");
        }
    }
}
