﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerRespawn : MonoBehaviour
{
    public SetActiveSlow _SetActiveSlow;
    private GameObject _LastRespawn;
    private Vector3 _LastPosition;
    private Quaternion _LastRotation;
    private Transform _Transform;
    private CharacterController _CharacterController;
    // Start is called before the first frame update
    void Start()
    {
        _Transform = GetComponent<Transform>();
        _CharacterController = GetComponent<CharacterController>();
        Messenger.AddListener("TheEnd", TheEnd);
    }

    // Update is called once per frame
    void Update()
    {   
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            SceneManager.LoadScene("StartMenu");
        }
        //Debug.Log(_CharacterController.velocity.y);
        if (_CharacterController.velocity.y < -21.0f)
        {
            Debug.Log("Fall");
            _SetActiveSlow.gameObject.SetActive(true);
            _SetActiveSlow.Respawn();
            StartCoroutine(LastRespawn());
        }
        //Debug.Log(_LastPosition.x);
    }
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Log");
        if ((other.gameObject.tag == "Respawn")&&(_LastRespawn!=other.gameObject))
        {
            Debug.Log("Collission");
            _LastRespawn = other.gameObject;
            _LastPosition = _Transform.position;
            _LastRotation = _Transform.rotation;
        }
    }
    public IEnumerator LastRespawn()
    {
        yield return new WaitForSeconds(0.9f);
        //_CharacterController.transform.position = _LastPosition;
        //_CharacterController.transform.rotation = _LastRotation;
        _CharacterController.enabled = false;
        _Transform.SetPositionAndRotation(_LastPosition, _LastRotation);
        _CharacterController.enabled = true;
    }
    public void TheEnd()
    {
        StartCoroutine(CurTheEnd());
    }
    public IEnumerator CurTheEnd()
    {
        yield return new WaitForSeconds(5.0f);
        _SetActiveSlow.gameObject.SetActive(true);
        yield return new WaitForSeconds(10.0f);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        SceneManager.LoadScene("StartMenu");
    }
}
