﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ManagerMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void InGame()
    {
        SceneManager.LoadScene("Level01");
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}
