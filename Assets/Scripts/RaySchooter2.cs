﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RaySchooter2 : MonoBehaviour
{

    private Camera _camera;

    public float ForceMultiplier;

    // Start is called before the first frame update
    void Start()
    {
        _camera = GetComponent<Camera>();
        

    }

    //метод автоматически запускается после прорисовки сцены
    private void OnGUI()
    {
        int size = 12;
        float posX = _camera.pixelWidth / 2 - size / 4;
        float posY = _camera.pixelHeight / 2 - size / 2;

        GUI.Label(new Rect(posX, posY, size, size), "*");
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetMouseButtonDown(0))
        {
            Shooting();
            Debug.Log("shoot");

            
        }
        
        
    }
    
    void Shooting()
    {
        Vector3 point = new Vector3(_camera.pixelWidth / 2, _camera.pixelHeight / 2, 0);//точка центра экрана - координаты экрана
        Ray ray = _camera.ScreenPointToRay(point);//создается луч из камеры
        RaycastHit hit;// переменная для записи результатов рейкаста

        if (Physics.Raycast(ray, out hit))
        {

            GameObject hitObject = hit.transform.gameObject;


            /* do not working
            var controlDirection : Vector3 = Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            var actualDirection = cameraTransform.TransformDirection(controlDirection);
            */


            if (hitObject.gameObject.GetComponent<Rigidbody>() != null)
            {
                //эта штука прибавдяет относительно самого объекта в который стреляю. а не относительно игрока.
                //как сделать относительно игрока?
                //hitObject.gameObject.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * ForceMultiplier);

                hitObject.gameObject.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * ForceMultiplier);

                //rigidbody.AddForce(Camera.main.transform.forward * forceStrength);
                //hitObject.gameObject.GetComponent<Rigidbody>().AddForce(Vector3.forward * ForceMultiplier);

            }


        }
    }
    

}
