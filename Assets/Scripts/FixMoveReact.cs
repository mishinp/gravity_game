﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixMoveReact : MonoBehaviour
{
    public bool fixX = false;
    public bool fixY = true;
    public bool fixZ = true;

    Vector3 firstPoint;
    Vector3 Direction;
    Vector3 startPosition;
    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ReactToHit(Vector3 hit)
    {
        Debug.Log("Hit");
        Direction = Vector3.ClampMagnitude(firstPoint - hit, 0.1f);
        Direction.x = (!fixX) ? Direction.x : 0;
        Direction.y = (!fixY) ? Direction.y : 0;
        Direction.z = (!fixZ) ? Direction.z : 0;
        transform.Translate(Direction);
        firstPoint = hit;
    }
}
