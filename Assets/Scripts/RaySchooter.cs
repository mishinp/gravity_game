﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RaySchooter : MonoBehaviour
{
    [SerializeField]
    private float forceMultiplier;

    private Camera _camera;

    private bool mousedown;

    // Start is called before the first frame update
    void Start()
    {
        _camera = GetComponent<Camera>();
        mousedown = false;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

    }

    //метод автоматически запускается после прорисовки сцены
    private void OnGUI()
    {
        int size = 12;
        float posX = _camera.pixelWidth / 2 - size / 4;
        float posY = _camera.pixelHeight / 2 - size / 2;

        //GUI.Label(new Rect(posX, posY, size, size), "*");
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            mousedown = true;
            //Debug.Log("mouse down");
        } 
        


        if (mousedown)
        {
            //Debug.Log("mouse down2");
            //mousedown = true;

            Vector3 point = new Vector3(_camera.pixelWidth / 2, _camera.pixelHeight / 2, 0);//точка центра экрана - координаты экрана
            Ray ray = _camera.ScreenPointToRay(point);//создается луч в сторону камеры
            RaycastHit hit;// переменная для записи результатов рейкаста

            if (Physics.Raycast(ray, out hit))
            {

                GameObject hitObject = hit.transform.gameObject;

                ReactiveMove target = hitObject.GetComponent<ReactiveMove>();//конектимся к объекту в который попали и получаем доступ к его скрипту 
                ReactiveRotate target2 = hitObject.GetComponent<ReactiveRotate>();//конектимся к объекту в который попали и получаем доступ к его скрипту
                ReactiveScale target3 = hitObject.GetComponent<ReactiveScale>();//конектимся к объекту в который попали и получаем доступ к его скрипту
                MoveTelekinesis target4 = hitObject.GetComponent<MoveTelekinesis>();//конектимся к объекту в который попали и получаем доступ к его скрипту
                Buses target5 = hitObject.GetComponent<Buses>();
                FixMoveReact target6 = hitObject.GetComponent<FixMoveReact>();

                if (target != null && target.gameObject.GetComponent<ReactiveMove>())
                {
                    
                    target.ReactToHit();//вызываем метод во внешнем скрипте
                   

                }

                else if (target2 != null && target2.gameObject.GetComponent<ReactiveRotate>())
                {
  
                    target2.ReactToHit_rotate(hit.point);//вызываем метод во внешнем скрипте
                    
                }

                else if (target3 != null && target3.gameObject.GetComponent<ReactiveScale>())
                {
                    target3.ReactToHit_scale();//вызываем метод во внешнем скрипте
                   
                }
                else if (target4 != null && target4.gameObject.GetComponent<MoveTelekinesis>())
                {
                    target4.ReactToHit();//вызываем метод во внешнем скрипте
                    
                }
                else if (target5 != null && target5.gameObject.GetComponent<Buses>())
                {
                    target5.ReactToHitBuses(hit.point);//вызываем метод во внешнем скрипте
                   
                }
                else if (target6 != null && target6.gameObject.GetComponent<FixMoveReact>())
                {
                    target6.ReactToHit(hit.point);//вызываем метод во внешнем скрипте
                    
                }

                else if (hitObject.gameObject.GetComponent<Rigidbody>() != null)
                {
                    hitObject.gameObject.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * forceMultiplier);


                }
            }

            if (Input.GetMouseButtonUp(0))
            {
                mousedown = !mousedown;
                // Debug.Log("mouse up");
            }

        }

        

        
    }





}
