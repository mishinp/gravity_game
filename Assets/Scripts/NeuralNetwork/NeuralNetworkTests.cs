﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeuralNetworkTests : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        var dataset = new List<Tuple<double, double[]>>
       {
           new Tuple<double, double[]>(0,new double[]{0,0,0,0}),
           new Tuple<double, double[]>(0,new double[]{0,0,0,1}),
           new Tuple<double, double[]>(1,new double[]{0,0,1,0}),
           new Tuple<double, double[]>(0,new double[]{0,0,1,1}),
           new Tuple<double, double[]>(0,new double[]{0,1,0,0}),
           new Tuple<double, double[]>(0,new double[]{0,1,0,0}),
           new Tuple<double, double[]>(1,new double[]{0,1,1,0}),
           new Tuple<double, double[]>(0,new double[]{0,1,1,1}),
           new Tuple<double, double[]>(1,new double[]{1,0,0,0}),
           new Tuple<double, double[]>(1,new double[]{1,0,0,1}),
           new Tuple<double, double[]>(1,new double[]{1,0,1,0}),
           new Tuple<double, double[]>(1,new double[]{1,0,1,1}),
           new Tuple<double, double[]>(1,new double[]{1,1,0,0}),
           new Tuple<double, double[]>(0,new double[]{1,1,0,1}),
           new Tuple<double, double[]>(1,new double[]{1,1,1,0}),
           new Tuple<double, double[]>(1,new double[]{1,1,1,1})
       };
       var topology = new Topology(4, 1,0.5, 2);
       var neuralNetwork = new NeuralNetwork(topology);
       var difference = neuralNetwork.Learn(dataset, 1000);

        var results = new List<double>();
        foreach (var data in dataset)
        {
            Debug.Log(neuralNetwork.FeedForward(data.Item2).Output);
            results.Add(neuralNetwork.FeedForward(data.Item2).Output);
        }
       

        var result = neuralNetwork.FeedForward();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
