﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomEnableCloud : MonoBehaviour
{

    public GameObject[] objects;

    [SerializeField] bool _enable;
    [SerializeField] private float _frequency;
    [SerializeField] private float _duration;


    void Start()
    {

        //enabled = true;
        //_frequency = 0.5f;
        //_duration = 5;

        if (_enable)
        {
            StartCoroutine(CoroutineActivate());
        }

    }

    void Update()
    {
        if (!_enable)
        {
            StopCoroutine(CoroutineActivate());
        }
    }


    IEnumerator CoroutineActivate()
    {
        while (true) {
            StartCoroutine(ObjectsActivate());
            yield return new WaitForSeconds(_frequency);
        }
    }

    
    IEnumerator ObjectsActivate(){

        int _objectID = Random.Range(0, objects.Length);
            Debug.Log(_objectID);
            objects[_objectID].SetActive(true);
            yield return new WaitForSeconds(_duration);
            objects[_objectID].SetActive(false);

        }

}
