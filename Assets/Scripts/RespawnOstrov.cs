﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnOstrov : MonoBehaviour
{
    public ForOstrovok _Ostrovok;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            _Ostrovok.SetStartPosition();
            _Ostrovok._Sterring = null;
        }
    }
}
