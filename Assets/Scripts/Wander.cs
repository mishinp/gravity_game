﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wander : Face
{
    public float offset;
    public float radius;
    public float rate;
    public override void Awake()
    {
        target = new GameObject();
        target.transform.position = transform.position;
        base.Awake();
    }
    public override Steering GetSteering()
    {
        Steering steering = new Steering();
        float wanderOrientation = Random.Range(-1.0f, 1.0f) * rate;
        float targetOrientation = wanderOrientation + agent.orientation;
        Vector3 orientationVec = OriToVec(targetOrientation);
        Vector3 targetPosition = (offset * orientationVec) + target.transform.position;
        //targetPosition = targetPosition + OriToVec(agent.orientation) * radius;
        target.transform.position = targetPosition;
        
        steering = base.GetSteering();
        steering.linear = target.transform.position - transform.position;
        Debug.Log(steering.linear);
        steering.linear.Normalize();
        steering.linear *= agent.maxAccel;
        return steering;
    }
}
