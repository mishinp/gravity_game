﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
//если нет этого компонента срипт работать не будет


[AddComponentMenu("Control Script/FPS Input")]
//добавление сценария в меню компонентов

public class FPSInput_jump : MonoBehaviour
{

    public float speed = 6.0f;
    private CharacterController _charController;
    public float gravity = -9.8f;


    //jump
    public float jumpSpeed = 12.0f;
    public float terminalVelocity = -8.0f;
    public float minFall = -1.5f;

    private float _vertSpeed;
    //jump end

    public const float baseSpeed = 6.0f;

    /*
    void Awake()
    {
        Messenger<float>.AddListener(GameEvent.SPEED_CHANGED, OnSpeedChanged);//система рассылки передает сообщение флоат
    }

    void OnDestroy()
    {
        Messenger<float>.RemoveListener(GameEvent.SPEED_CHANGED, OnSpeedChanged);
    }

    private void OnSpeedChanged(float value)
    {
        speed = baseSpeed * value;
    }
    */


    // Start is called before the first frame update
    void Start()
    {
        _charController = GetComponent<CharacterController>();

        _vertSpeed = minFall;//jump
        UnityEngine.Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        float deltaX = Input.GetAxis("Horizontal") * speed;
        float deltaZ = Input.GetAxis("Vertical") * speed;

        Vector3 movement = new Vector3(deltaX, 0, deltaZ);

        movement = Vector3.ClampMagnitude(movement, speed);
        movement.y = gravity;

        //jump
        if (_charController.isGrounded)
        {
            if (Input.GetButtonDown("Jump"))
            {
                _vertSpeed = jumpSpeed;
            }
            else
            {
                _vertSpeed = minFall;
            }
        }
        else
        {
            _vertSpeed += gravity * 5 * Time.deltaTime;
            if(_vertSpeed < terminalVelocity)
            {
                _vertSpeed = terminalVelocity;
            }
        }

        movement.y = _vertSpeed;






        movement *= Time.deltaTime;
        movement = transform.TransformDirection(movement);
        _charController.Move(movement);


        /* 
         * transform.Translate(deltaX * Time.deltaTime, 0, deltaY * Time.deltaTime);
         //дельта тайм нужна для того чтобы зависить от времени и работать с равной скорость на разных компах, с разной FPS
         // важна хреновина
         */
    }
}