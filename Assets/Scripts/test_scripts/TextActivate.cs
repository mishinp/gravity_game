﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextActivate : MonoBehaviour

{

    // [SerializeField] private Vector3 dPos;

    private bool _open;

   

    private void Operate()
    {
        /*if (_open)
        {
            Vector3 pos = transform.position - dPos;
            transform.position = pos;
        }

        else
        {
            Vector3 pos = transform.position + dPos;
            transform.position = pos;
        }*/

        _open = !_open;
    }
    
    public void Activate()
    {
        

         if (!_open)
        {
            //Vector3 pos = transform.position + dPos;
           // transform.position = pos;
            _open = true;
            Debug.Log("activate");
            
            this.gameObject.GetComponent<MeshRenderer>().enabled = false;
            //iTween.FadeTo(gameObject, iTween.Hash("alpha", 0, "time", 1));
        }
    }

    public void Deactivate()
    {
        

        if (_open)
        {
            //Vector3 pos = transform.position - dPos;
            //transform.position = pos;
            _open = false;
            Debug.Log("Deactivate");
            
            this.gameObject.GetComponent<MeshRenderer>().enabled = true;
            //iTween.FadeTo(gameObject, iTween.Hash("alpha", 1, "time", 1));
        }

    }
}