﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshCollider))]

public class GizmosController_v1 : MonoBehaviour
{

    private Vector3 _screenPoint;
    private Vector3 _offset;

    [SerializeField] bool freezeRotation;
    [SerializeField] bool x;
    [SerializeField] bool y;
    [SerializeField] bool z;


    void OnMouseDown()

    {
        
        //заморозка врещения объекта
        if (freezeRotation)
        {
            GetComponent<Rigidbody>().freezeRotation = true;
        }   
            
        //Transforms position from world space into screen space.
        _screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);

        _offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z));

    }

    void OnMouseDrag()
    {
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z);

        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + _offset;
        
        transform.position = curPosition;

    }


    //возможность отключить вращение -- готово
    //возможность выбрать одну нужную ось
    //возможность задать ограничения перемещениям

}