using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cloudRotate : MonoBehaviour
{
    public float sensitivity = 1f;
    public float acceleration = 0.2f;
    public float rotationSpeed = 5f;

    private float accelerationX;
    private float mouseX;

    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            mouseX = Input.mousePosition.x;
        }


        transform.Rotate(new Vector3(0, accelerationX - rotationSpeed * Time.deltaTime, 0), Space.World);

        accelerationX = Mathf.Lerp(accelerationX, 0, Time.deltaTime / acceleration);

        if (Input.GetMouseButton(0))
        {
            float deltaX = mouseX - Input.mousePosition.x;
            accelerationX = deltaX * sensitivity;
            mouseX = Input.mousePosition.x;
        }
    }
}