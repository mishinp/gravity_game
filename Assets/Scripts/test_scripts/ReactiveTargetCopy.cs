﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactiveTargetCopy : MonoBehaviour
{
    
    
    private Vector3 screenPosition;
    private bool isMouseDrag;
    private GameObject target;
    private Vector3 offset;

    public void Awake()
    {
        target = this.gameObject;
    }

    public void ReactToHit()
    {

        

        if (Input.GetMouseButtonDown(0)) {

            isMouseDrag = true;
           
            //Convert world position to screen position.
            screenPosition = Camera.main.WorldToScreenPoint(target.transform.position);
            offset = target.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPosition.z));

        }

        if (Input.GetMouseButtonUp(0))
        {
            isMouseDrag = false;
            Debug.Log(isMouseDrag);
        }

        if (isMouseDrag)
        {

            //track mouse position.
            Vector3 currentScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPosition.z);

            //convert screen position to world position with offset changes.
            Vector3 currentPosition = Camera.main.ScreenToWorldPoint(currentScreenSpace) + offset;

            
            //It will update target gameobject's current postion.
            target.transform.position = currentPosition;
        }


    }
    
   
    
    
}
