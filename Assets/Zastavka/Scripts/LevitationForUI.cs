﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevitationForUI : MonoBehaviour
{
    public float _Amplitude = 5.0f;

    private float _StartPosY;
    private RectTransform _RectTransform;
    private float _Shift;
    // Start is called before the first frame update
    void Start()
    {
        _RectTransform = GetComponent<RectTransform>();
        _StartPosY = _RectTransform.position.y;
        _Shift = Random.Range(0.0f, 5.0f);
    }

    // Update is called once per frame
    void Update()
    {
        _RectTransform.position = new Vector3(_RectTransform.position.x, _StartPosY + _Amplitude * Mathf.Cos(Time.time + _Shift), 0.0f);
    }
}
